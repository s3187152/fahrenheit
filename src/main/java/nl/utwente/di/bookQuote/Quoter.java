package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> books = new HashMap<String, Double>() {{
        put("1", 10.0);
        put("2", 45.0);
        put("3", 20.0);
        put("4", 35.0);
        put("5", 50.0);
    }};

    public double getBookPrice(String isbn){
        return books.getOrDefault(isbn, 0.0);
    }
}
